# Licensor
A free software to prepend GPL-3.0 copyright header into the top of an Ada source file.

## Usage
`./obj/licensor <ADA_SOURCE_FILE>`

## Building
### Dependencies
* gnat >= 5 (Ada 2012)
* gprbuild (GPL 2017) 

Go to the root directory of the project and run `gprbuild -p` to build the software.

## Authors
* [Momozor](https://github.com/momozor)

## License
This software is released under the GPL-3.0 license. See LICENSE file for more
details.
