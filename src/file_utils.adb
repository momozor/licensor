--  This file is part of Licensor.

--  Copyright (C) 2019  Momozor

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with Ada.Text_IO;
with Ada.Text_IO.Unbounded_IO;
with Ada.Strings.Unbounded;
with Ada.Directories;
with Ada.Characters.Latin_1;

package body File_Utils is
   package IO renames Ada.Text_IO;
   package IOU renames Ada.Text_IO.Unbounded_IO;
   package SO renames Ada.Strings.Unbounded;
   package DIR renames Ada.Directories;
   package CHARS renames Ada.Characters.Latin_1;

   -- get source file original content
   function Get_Original_Content
     (Source_File : in String) return SO.Unbounded_String
   is
      F                : IO.File_Type;
      Original_Content : SO.Unbounded_String;
   begin
      IO.Open (File => F, Mode => IO.In_File, Name => Source_File);

      while not IO.End_Of_File (F) loop
         SO.Append (Original_Content, IO.Get_Line (F));
         SO.Append (Original_Content, CHARS.LF);
      end loop;
      IO.Close (F);
      return Original_Content;
   end Get_Original_Content;

   procedure Prepend_To_File (Source_File : in String) is
      F      : IO.File_Type;
      Result : SO.Unbounded_String :=
        Get_Original_Content (Source_File => Source_File);
   begin

      DIR.Delete_File (Source_File);

      IO.Create (File => F, Mode => IO.Out_File, Name => Source_File);
      IO.Put_Line (F, "--  This file is part of PROJECT");
      IO.New_Line (F, 1);
      IO.Put_Line (F, "--  Copyright (C) 2019 Momozor");
      IO.New_Line (F, 1);
      IO.Put_Line
        (F,
         "--  This program is free software: you can redistribute it and/or modify");
      IO.Put_Line
        (F,
         "--  it under the terms of the GNU General Public License as published by");
      IO.Put_Line
        (F,
         "--  the Free Software Foundation, either version 3 of the License, or");
      IO.Put_Line (F, "--  (at your option) any later version.");
      IO.New_Line (F, 1);
      IO.Put_Line
        (F,
         "--  This program is distributed in the hope that it will be useful,");
      IO.Put_Line
        (F,
         "--  but WITHOUT ANY WARRANTY; without even the implied warranty of");
      IO.Put_Line
        (F,
         "--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
      IO.Put_Line (F, "--  GNU General Public License for more details.");
      IO.New_Line (F, 1);
      IO.Put_Line
        (F,
         "--  You should have received a copy of the GNU General Public License");
      IO.Put_Line
        (F,
         "--  along with this program.  If not, see <https://www.gnu.org/licenses/>.");
      IO.New_Line (F, 1);
      IO.Close (F);

      IO.Open (File => F, Mode => IO.Append_File, Name => Source_File);
      IO.Put_Line (F, SO.To_String (Result));
      IO.Close (F);

   end Prepend_To_File;
end File_Utils;
