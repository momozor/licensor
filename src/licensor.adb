--  This file is part of Licensor.

--  Copyright (C) 2019  Momozor

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with Ada.Command_Line;
with Ada.Text_IO;
with File_Utils;

procedure Licensor is
   package CL renames Ada.Command_Line;
   package IO renames Ada.Text_IO;
begin
   File_Utils.Prepend_To_File (Source_File => CL.Argument (1));
exception
   when Constraint_Error =>
      IO.Put_Line ("You need to pass source file path/name as argument");
end Licensor;
